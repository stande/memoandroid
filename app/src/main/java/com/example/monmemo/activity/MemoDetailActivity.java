package com.example.monmemo.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

import com.example.monmemo.R;
import com.example.monmemo.constants.Constant;

public class MemoDetailActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_memo_detail);
        String memoParam = getIntent().getStringExtra(Constant.PARAM_MEMO_DETAIL);
        TextView textView = findViewById(R.id.nom_memo);
        textView.setText(memoParam);
    }
}

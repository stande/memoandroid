package com.example.monmemo.dto;

public class MemoWSDTO {
    private String origin;
    private String url;
    private MemoDTO form;

    public MemoWSDTO() {
    }

    public MemoWSDTO(String origin, String url, MemoDTO form) {
        this.origin = origin;
        this.url = url;
        this.form = form;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public MemoDTO getForm() {
        return form;
    }

    public void setForm(MemoDTO form) {
        this.form = form;
    }

    @Override
    public String toString() {
        return "Memo \n\n" +
                "ID : " + form.memoId + "\n\n" +
                "Nom : " + form.nom + "\n\n" +
                "Origin WS : " + origin + "\n\n" +
                "URL WS : " + url;
    }
}

package com.example.monmemo.dto;

import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

@Entity(tableName = "memos")
public class MemoDTO {

    @PrimaryKey(autoGenerate = true)
    public long memoId = 0;
    public String nom;

    // Exemple d'attribut non pris en compte par Room :
    //public boolean selectionne;

    // Constructeur public vide (obligatoire si autre constructeur existant) :
    public MemoDTO() {
    }

    // Autre constructeur :
    @Ignore
    public MemoDTO(String nom) {
        this.nom = nom;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    @Override
    public String toString() {
        return "Memo \n\n" +
                "ID : " + memoId + "\n\n" +
                "Nom : " + nom;
    }
}

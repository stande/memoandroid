package com.example.monmemo.constants;

public class Constant {
    //private final String NO_INTERNET = "Vous n'êtes pas connecté à internet";
    public final static String ERREUR_WS = "Erreur à l'appel du WebService";
    public final static String PARAM_WS_MEMO_ID = "memoId";
    public final static String PARAM_WS_MEMO_NOM = "nom";
    public final static String PARAM_MEMO_DETAIL = "memo_param";
    public final static String FRAGMENT_MEMO_DETAIL = "memo_detail";
    public final static String PARAM_LAST_ITEM_CLICK = "lastItemClick";
}

package com.example.monmemo;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.example.monmemo.adapter.MemosAdapter;
import com.example.monmemo.constants.Constant;
import com.example.monmemo.database.AppDatabaseHelper;
import com.example.monmemo.dto.MemoDTO;
import com.example.monmemo.helper.ItemTouchHelperCallback;

import java.util.List;

public class MainActivity extends AppCompatActivity {
    private MemosAdapter memosAdapter;
    private List<MemoDTO> listMemos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        RecyclerView recyclerView = findViewById(R.id.liste_memos);

        // Recuperer et afficher la derniere position cliquee
        SharedPreferences preferences =
                PreferenceManager.getDefaultSharedPreferences(this);
        int position = preferences.getInt(Constant.PARAM_LAST_ITEM_CLICK, 0);
        if (position != 0) {
            Toast.makeText(this, "Position : " + position, Toast.LENGTH_LONG).show();
        }

        // Declaration de la BDD + il faut au moins une requete pour initialiser la bdd (à l'init de la liste ici)
        AppDatabaseHelper.getDatabase(this);

        // à ajouter pour de meilleures performances :
        recyclerView.setHasFixedSize(true);

        // layout manager, décrivant comment les items sont disposés :
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

        // récupérer une liste de courses :
        listMemos = AppDatabaseHelper.getDatabase(this)
                .memosDAO()
                .getListeMemos();

        // adapter :
        memosAdapter = new MemosAdapter(listMemos, this);
        recyclerView.setAdapter(memosAdapter);

        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(
                new ItemTouchHelperCallback(memosAdapter));
        itemTouchHelper.attachToRecyclerView(recyclerView);
    }

    public void addToList(View view) {
        EditText editText = findViewById(R.id.nom_liste);
        MemoDTO memo = new MemoDTO(editText.getText().toString());
        // Ajouter un memo en BDD
        AppDatabaseHelper.getDatabase(this).memosDAO().insert(memo);
        // Ajoute le memo dans la liste en 1ere position
        listMemos.add(0, memo);
        memosAdapter.notifyItemInserted(0);
        // Retour texte en blanc
        editText.setText("");
    }
}

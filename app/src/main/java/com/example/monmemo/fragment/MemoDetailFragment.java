package com.example.monmemo.fragment;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.monmemo.R;
import com.example.monmemo.constants.Constant;


/**
 * A simple {@link Fragment} subclass.
 */
public class MemoDetailFragment extends Fragment {

    public MemoDetailFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_memo_detail, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (getArguments() != null && getView() != null) {
            String memoParam = getArguments().getString(Constant.PARAM_MEMO_DETAIL);
            TextView textView = getView().findViewById(R.id.text_memo_detail);
            textView.setText(memoParam);
            //Toast.makeText(getContext(), "argument = " + Integer.toString(argument), Toast.LENGTH_LONG).show();
        }
    }
}

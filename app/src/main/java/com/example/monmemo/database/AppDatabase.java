package com.example.monmemo.database;

import androidx.room.Database;
import androidx.room.RoomDatabase;

import com.example.monmemo.dao.MemosDAO;
import com.example.monmemo.dto.MemoDTO;

@Database(entities = {MemoDTO.class}, version = 1)
public abstract class AppDatabase extends RoomDatabase {
    public abstract MemosDAO memosDAO();
}

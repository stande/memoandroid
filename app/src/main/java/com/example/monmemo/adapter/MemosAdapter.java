package com.example.monmemo.adapter;

import com.example.monmemo.R;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import com.example.monmemo.activity.MemoDetailActivity;
import com.example.monmemo.constants.Constant;
import com.example.monmemo.database.AppDatabaseHelper;
import com.example.monmemo.dto.MemoDTO;
import com.example.monmemo.dto.MemoWSDTO;
import com.example.monmemo.fragment.MemoDetailFragment;
import com.example.monmemo.helper.ConnectivityHelper;
import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import java.util.Collections;
import java.util.List;

import cz.msebera.android.httpclient.Header;

public class MemosAdapter extends RecyclerView.Adapter<MemosAdapter.MemoViewHolder> {
    // Liste d'objets métier :
    private List<MemoDTO> listeMemos;
    private AppCompatActivity activity;

    // Constructeur :
    public MemosAdapter(List<MemoDTO> listeMemos, AppCompatActivity activity) {
        this.listeMemos = listeMemos;
        this.activity = activity;
    }

    @Override
    public MemoViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View viewMemo = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.memo_item_liste, parent, false);
        return new MemoViewHolder(viewMemo);
    }

    @Override
    public void onBindViewHolder(MemoViewHolder holder, int position) {
        holder.textViewNomMemo.setText(listeMemos.get(position).nom);
    }

    @Override
    public int getItemCount() {
        return listeMemos.size();
    }

    // Appelé à chaque changement de position, pendant un déplacement.
    public boolean onItemMove(int positionDebut, int positionFin) {
        Collections.swap(listeMemos, positionDebut, positionFin);
        notifyItemMoved(positionDebut, positionFin);
        return true;
    }

    // Appelé une fois à la suppression.
    public void onItemDismiss(int position) {
        if (position > -1) {
            listeMemos.remove(position);
            notifyItemRemoved(position);
        }
    }

    public class MemoViewHolder extends RecyclerView.ViewHolder {
        // TextView intitulé course :
        private TextView textViewNomMemo;

        // Constructeur :
        private MemoViewHolder(View itemView) {
            super(itemView);
            textViewNomMemo = itemView.findViewById(R.id.nom_memo);

            // listener :
            textViewNomMemo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    MemoDTO memo = listeMemos.get(getAdapterPosition());

                    // Appel WS
                    if (ConnectivityHelper.estConnecte(activity)) {
                        getMemoWebService(activity, memo);
                    } else {
                        launchInterface(activity, memo.toString());
                        //Toast.makeText(activity, NO_INTERNET, Toast.LENGTH_LONG).show();
                    }

                    // Recuperer la derniere position cliquée
                    int position = getAdapterPosition() + 1;
                    saveToSharedPreference(activity, position);
                    //Toast.makeText(activity, "Position : " + position, Toast.LENGTH_LONG).show();

                }
            });
        }

        private void getMemoWebService(final Context context, MemoDTO memo) {
            // client HTTP :
            AsyncHttpClient client = new AsyncHttpClient();

            // paramètres :
            RequestParams requestParams = new RequestParams();
            requestParams.put(Constant.PARAM_WS_MEMO_ID, memo.memoId);
            requestParams.put(Constant.PARAM_WS_MEMO_NOM, memo.nom);

            // appel :
            client.post("https://www.httpbin.org/post", requestParams, new AsyncHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers,
                                      byte[] response) {
                    // retour du webservice :
                    String retour = new String(response);
                    //Log.i("TAG_WS", retour);
                    // conversion en un objet Java (à faire!) ayant le même format que le JSON :
                    Gson gson = new Gson();
                    MemoWSDTO memoWSDTO = gson.fromJson(retour, MemoWSDTO.class);
                    // affichage d'un attribut :
                    //Toast.makeText(context, "MemoWSDTO : " + memoWSDTO.toString(), Toast.LENGTH_LONG).show();
                    launchInterface(context, memoWSDTO.toString());
                }

                @Override
                public void onFailure(int statusCode, Header[] headers,
                                      byte[] errorResponse, Throwable e) {
                    //Log.e("TAG_WS", e.toString());
                    //Toast.makeText(context, Constant.ERREUR_WS, Toast.LENGTH_LONG).show();
                    launchInterface(context, Constant.ERREUR_WS);
                }
            });
        }

        private void saveToSharedPreference(final Context context, int position) {
            SharedPreferences preferences =
                    PreferenceManager.getDefaultSharedPreferences(context);
            SharedPreferences.Editor editor = preferences.edit();
            editor.putInt(Constant.PARAM_LAST_ITEM_CLICK, position);
            editor.apply();
        }

        private void launchInterface(final Context context, String memoToString) {
            if (activity.findViewById(R.id.conteneur_memo_detail) == null) {
                Intent intent = new Intent(context, MemoDetailActivity.class);
                intent.putExtra(Constant.PARAM_MEMO_DETAIL, memoToString);
                context.startActivity(intent);
            } else {
                // fragment :
                MemoDetailFragment fragment = new MemoDetailFragment();
                // Parametre
                Bundle bundle = new Bundle();
                bundle.putString(Constant.PARAM_MEMO_DETAIL, memoToString);
                fragment.setArguments(bundle);
                // fragment manager :
                FragmentManager fragmentManager = activity.getSupportFragmentManager();
                // transaction :
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.conteneur_memo_detail, fragment, Constant.FRAGMENT_MEMO_DETAIL);
                fragmentTransaction.commit();
            }
        }

        public TextView getTextViewNomMemo() {
            return textViewNomMemo;
        }

        public void setTextViewNomMemo(TextView textViewNomMemo) {
            this.textViewNomMemo = textViewNomMemo;
        }
    }
}
package com.example.monmemo.dao;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.example.monmemo.dto.MemoDTO;

import java.util.List;

@Dao
public abstract class MemosDAO {

    @Query("SELECT * FROM memos ORDER BY memoId DESC")
    public abstract List<MemoDTO> getListeMemos();

    @Query("SELECT COUNT(*) FROM memos WHERE nom = :nom")
    public abstract long countMemosParNom(String nom);

    @Insert
    public abstract void insert(MemoDTO... memos);

    @Update
    public abstract void update(MemoDTO... memos);

    @Delete
    public abstract void delete(MemoDTO... memos);
}
